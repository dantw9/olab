# Antohny O'Donoghue Lab website

## About

This is my very first NextJS website created for Antohny O'Donoghue's laboratory at University of California San Diego

## Built With

- Javascript
- Typescript
- NextJS
- SASS


